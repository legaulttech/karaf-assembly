# Apache Karaf Custom Build

Apache Karaf custom flavor build which includes

1. WAR
2. JDBC
3. MySQL
4. WebConsole


## Getting Started

Make sure the following software is installed on the clients machine.

1. Install [Maven](https://maven.apache.org/download.cgi)
2. Download [Java](https://java.com/en/download/)

Clone the repository and navigate within the build/apache-karaf folder. Once located within the folder run the following command:

`mvn clean install`

Now navigate within the `target` folder and notice that there is two zip files: 

1. ZIP - Windows environment 
2. TAR.GZ - Linux/OSX environment 

Now you have a fresh Apache Karaf server loaded with features that are already needed.

## Starting the Server

### Starting with Command Prompt

In order to start the server with the console run the following commands:

1. Unzip the necessary file
2. Make sure execute permissions are placed on the bin/* folder
3. Run the following command `./bin/karaf`
  * This will open the command prompt for the Karaf server
  * The server will respond on port *8181*
  * Username: *karaf*
  * Password: *karaf*
4. Run the following command `feature:list`
  * This will list all the features that are available, what is active, what is disabled
5. For a more detailed list of commands that are available read more [here](https://karaf.apache.org/manual/latest/quick-start.html)

### Starting the Server

1. Unzip the necessary file
2. Make sure execute permissions are placed on the bin/* folder
3. Run the following command `./bin/start`
  * The server will respond on port *8181*
  * Username: *karaf*
  * Password: *karaf*
4. Now the server is running in the background `ps -ef | grep java`
5. This will list the java process running Karaf

## Editing the Code

In order to add more functionality to the server on start up open the POM.xml file. Locate the `org.apache.karaf.tooling` plugin and locate the `bootFeatures`. These are features that will be pulled into the server during build time. 

In order to locate features that can be installed during build time follow these steps:

1. Start the server with a command prompt
2. Run `feature:list`
3. Read through the feature list and locate any features that could be used next time
4. Once located add them to the `bootFeatures` XML tag
5. Re-run `mvn clean install` and this will rebuild the server with the installed features

## JDBC

Once all the necessary JDBC features are installed create a database server, such as MySQL. Then run the following command in the Karaf command prompt: 

```
jdbc:ds-create -dn mysql -dbName [database_Name] -url "jdbc:mysql://[server_dns_Name]:[port]/[database]" -u [username] -p [password] [service_Name]
```

* [database_Name] - karaf
* [server_dns_Name] - 127.0.0.1
* [port] - 3306
* [database] - karaf (the database to connect too)
* [username] - root
* [password] - password
* [service_Name] - karaf-mysql (name of the service)

Now users can query the state of the driver through the console:
```
jdbc:ds-info [service_Name]
```

* [service_name]: karaf-mysql (name of the service created)

This will create the driver in the server and can be used to test the connection from the server to the database. Read more about this [here](https://svn.apache.org/repos/asf/karaf/site/production/manual/latest/jdbc.html)

## Authors

* **Patrique Legault** - *[Legault.Tech](http://legaulttech.com)*

## License

This project is licensed under the Apache 2.0 License - see the [here](https://www.apache.org/licenses/LICENSE-2.0) for details

## Acknowledgments

* Hat tip to anyone who's code was used
* [Apache Foundation](https://www.apache.org/)
